
def prefzero(A):
     
        lenA = len(A)
        psum = [0] * (lenA+1)
        pzero = [0] * (lenA+1)
        for k in range(1, lenA + 1):
            psum[k] = psum[k - 1] + A[k - 1]
            pzero[k] = k - psum[k]
            #print (k,psum[k], pzero[k])
        return pzero
        

def solution(A):
    
    lenA = len(A)
    n_passing = 0
    
    pzero = prefzero(A)
    
    for i in range(1,lenA):
        if A[i]:    
            n_passing += pzero[i]
            
    #print n_passing
    if n_passing > 1000000000:
        return -1
    return n_passing
    
    