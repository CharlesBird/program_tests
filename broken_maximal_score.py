# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    
    lenA = len(A)
    
    score = A[0]    # lead element
    i = 1
    
    if lenA < 20:
        # print ('A=',A)
        pass
    while i < lenA:
        # print ("len: ",lenA,"i: ", i, "looking over: ",min(i+6,lenA-1))
        ## do we want max or set of positive values, or smallest negative
        
        subarr = A[i:min(i+6,lenA-i)]
        # print('subarr=', subarr)
        if not subarr:
            break
        
        if A[i+1] > 0:  # add all +ve values
            
            i += 1
            score += A[i]
            # print('adding next +ve value A({:d}): {%d}'.format(i,A[i]) )
        ## should actually find next +ve value or the following
        else:
            # print('handling -ve value at i={:d}'.format(i))
            maxj = 0
            maxv = -10001
            for j in range(0, len(subarr)):
                if subarr[j]>0:
                    maxj= j
                    maxv = subarr[j]
                 #   print('+ve {:d} at {:d} in subarr'.format(maxv, maxj))
                    break ## needs check
                elif subarr[j]>maxv:
                    maxv = subarr[j]
                    maxj = j
                else:
                  #  print('fallthrough')
                  pass
            i += maxj
            score += maxv
        
                
    score += A[lenA-1]  # terminating condition
    return score