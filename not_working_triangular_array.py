# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(arr):
    lenarr = len(arr)
    sortarr = arr[:]
    sortarr.sort()
    #print (arr)
    #print (sortarr)

    ## we are ignoring index constraint at the moment

    while len(sortarr) > 3:    
            
        C = sortarr[-1]
        B = sortarr[-2]
        A = sortarr[-3]
        #print(C,B,A)
        
        if C<0 or B<0 or A<0:
            return 0
        if C < (A+B):
            return 1
        del sortarr[-1]

    return 0
    