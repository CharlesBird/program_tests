# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def daily_change(A):
    lenA = len(A)
    change = [0]*(lenA)
    
    for i in range (1,lenA):
        change[i-1] = A[i]-A[i-1]
    return change
    
def max_slice(A):

    max_ending = max_slice = 0

    for a in A:
        max_ending = max(0, max_ending + a)
        max_slice = max(max_slice, max_ending)

    return max_slice

def solution(A):
    
    lenA = len(A)
    
    daily= daily_change(A)
    
    #print (daily)
    
    #print(max_slice(daily))
    
    return max_slice(daily)