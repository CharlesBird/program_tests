# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(S):
    
    stack = list()
    
    for op in S.split(' '):
        
        if 0 == len(op):
            return -1   # empty command list, implictly stack empty at end
        
        if 'POP' == op:
            if len(stack)>0:
                del stack[-1]
            else:
                return -1   # empty stack
            pass
        elif 'DUP' == op:
            if len(stack)>0:
                stack.append(stack[-1])
            else:
                return -1   # empty stack
        elif '+' == op:
            if len(stack)> 1:
                a = stack[-1]
                b = stack[-2]
                del stack[-2:]
                stack.append( a + b )
            else:
                return -1
        elif '-' == op:
            if len(stack)> 1:
                a = stack[-1]
                b = stack[-2]
                del stack[-2:]
                stack.append( a - b )
                if stack[-1] >= 0:      ## should throw error here, not at end
                    return stack[-1]
            else:
                return -1
        else:   # number
            stack.append(int(op))
            
    
    if 0 == len(stack):
        return -1
    if stack[-1] >= 0:
        return stack[-1]
    else:
        return -1
    