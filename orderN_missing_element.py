# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

# this has array of N elements, containing all values bar one from 1:N+1 
# find missing value

def solution(A):

    lenA = len(A)
    if 0 == lenA:
        return 1
    
    # use equation for sum of arithmetic series
    
    sum_seq=(lenA+2)*((lenA+1)/float(2))    ## remember 2.x division
        
    return int(sum_seq - sum(A))    ## sum_seq will be float