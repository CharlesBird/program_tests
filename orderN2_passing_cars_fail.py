# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    
    lenA = len(A)
    n_passing = 0
    
    for i in range(0,lenA):
        for j in range(i+1, lenA):
            
            
            if 0==A[i] and 1 == A[j]:
                #print(i,j,'=', A[i], A[j], '*')
                n_passing += 1
            else:
                #print(i,j,'=', A[i], A[j])
                pass

    #print n_passing
    if n_passing > 1000000000:
        return -1
    return n_passing