    
#
# various code fragments for algo tests
#

def prefix_sums(A):
    lenA = len(A)
    prefsum = [0] * (lenA+1)
    for k in range(1, lenA + 1):
        prefsum[k] = prefsum[k - 1] + A[k - 1]
    return prefsum
    
def slice_total(arr_prefix_sum, x, y):
    return (arr_prefix_sum[y+1] - arr_prefix_sum[x])
    
def max_collect(A, start_point, n_moves):
    lenA = len(A)
    result = 0
    prefsum = prefix_sums(A)
    
    for pos in range( 0, min(start_point, n_moves) + 1):
        left_pos = start_point - pos
        right_pos = min( lenA - 1, max( start_point, start_point + n_moves - 2 * pos ))
        result = max(result, slice_total(prefsum, left_pos, right_pos))

    for pos in range(0, min(n_moves + 1, lenA - start_point)):
        right_pos = start_point + pos
        left_pos = max(0, min(start_point, start_point - (n_moves - 2 * pos)))
        result = max(result, slice_total(prefsum, left_pos, right_pos))

    return result    
    
if "__main__" == __name__:
    
    test_arr = [0,1,2,3,4,5]
    psum = prefix_sums(test_arr)
    print (psum ) # expect 15
    print (slice_total(psum, 3, 5 ) )   # expect 12
    mush_arr = [2,3,7,5,1,3,9]
    print(max_collect(mush_arr,4,6)) # expect 25