

def golden_max_slice(A):

    max_ending = max_slice = 0

    for a in A:
        max_ending = max(0, max_ending + a)
        max_slice = max(max_slice, max_ending)

    return max_slice

def main():
    AA = [ 0,-1, 5,6,-9,10,-11,9,2,3]
    
    print(AA)
    print(golden_max_slice(AA))

if '__main__' == __name__:
    main()
