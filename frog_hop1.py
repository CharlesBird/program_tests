

def solution(X, Y, D):
    # write your code in Python 2.7
    if X==Y:
        return 0
    if (Y-X) <= D:
        return 1
    ## remember 2.x has strange / behaviour    
    ## round goes up on 0.5, so this is a kludge on precision
    #raw = (Y-X)/float(D)
    #print( raw, round(0.4999+raw, 0), int(round(0.4999+raw,0)) )
    return int (round (0.4999+((Y-X)/float(D)), 0))
    

