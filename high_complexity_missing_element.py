
## missing element, but O(N^2)

# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    
    if 0 == len(A):
        return 1
    for i in range(1, len(A)+2): # range doesn't have end in it
        if i not in A:
            return i

    return 0    # fail