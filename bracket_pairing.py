

# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(S):
    
    q = list()
    
    if not S:
        return 1
    
    for s in S:
        if s in ['(','[','{']:
            q.append(s)
        if s in [')',']','}'] and 0 == len(q):
            return 0
        if ']' == s:
            if '[' == q[-1]:
                q.pop()
        if ')' == s:
            if '(' == q[-1]:
                q.pop()
        if '}' == s:
            if '{' == q[-1]:
                q.pop()
                
    if 0 == len(q):
        #print (q)
        return 1
    else:
        #print (q)
        return 0

