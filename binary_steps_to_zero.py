# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"


### need to fix bug, probably in MSB / 2s complement handling
def solution(S):
    n_steps = 0
    
    s = S.lstrip('0')
    
    for i in range(0,len(s)):
        if s[i] == '1':
            n_steps += 2
        else:
            n_steps += 1
    
    return n_steps-1
            