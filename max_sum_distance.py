# you can write to stdout for debugging purposes, e.g.
# print "this is a debug message"

def solution(A):
    
    lenA = len(A)
    cur_max_idx = 0
    max_sumd = -2500000000
    
    for i in range(0,lenA):
    
        max_sumd= max(max_sumd, A[i]+i+A[cur_max_idx]-cur_max_idx)
        #    print(i, cur_max_idx, A[i]+i+A[cur_max_idx]-cur_max_idx)

        if A[i]-i >= A[cur_max_idx]-cur_max_idx:
            #   print("new cur", cur_max_idx)
            cur_max_idx = i
    
    #   print(cur_max_idx,A[cur_max_idx])    
    max_sumd= max(max_sumd, 2*A[cur_max_idx]) # same idx case
    return max_sumd

    